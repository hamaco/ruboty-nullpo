module Ruboty
  module Handlers
    class Nullpo < Base
      on(
        /ぬるぽ/,
        name: "nullpo",
        description: "ぬるぽ ガッ!",
        all: true,
      )

      def nullpo(message)
        message.reply("ガッ!")
      end
    end
  end
end
