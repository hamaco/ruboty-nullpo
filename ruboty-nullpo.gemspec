lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "ruboty/nullpo/version"

Gem::Specification.new do |spec|
  spec.name     = "ruboty-nullpo"
  spec.version  = Ruboty::Nullpo::VERSION
  spec.authors  = ["hamaco"]
  spec.email    = ["hamanaka.kazuhiro@gmail.com"]
  spec.summary  = "ヌルポ ガッ!"
  spec.homepage = "https://bitbucket.org/hamaco/ruboty-nullpo"
  spec.license  = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_dependency "ruboty"
end
